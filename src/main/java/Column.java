import java.util.ArrayList;

public class Column<T> {
    public String label;
    public ArrayList<T> elements;

    public Column(){
        label = "";
        elements = new ArrayList<>();
    }
}
