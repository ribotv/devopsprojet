import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.InputMismatchException;

public class Dataframe {
    public ArrayList<Column> columns;

    public Dataframe(){
        columns = new ArrayList<Column>();
    }

    public Dataframe(ArrayList<String> labels, ArrayList data, int rowNumber){
        columns = new ArrayList<Column>();
        Column col;
        for (int i=0; i<labels.size(); i++){
            String type = data.get(i).getClass().getSimpleName();
            if ("Integer[]".equals(type)) {

                col = new Column<Integer>();
                Integer[] tmpData = (Integer[]) data.get(i);
                col.label = labels.get(i);
                col.elements = new ArrayList<Integer>();
                for (int j=0; j<rowNumber; j++){
                    col.elements.add(tmpData[j]);
                }
                columns.add(col);

            } else if ("String[]".equals(type)) {
                col = new Column<String>();
                String[] tmpData = (String[]) data.get(i);
                col.label = labels.get(i);
                col.elements = new ArrayList<String>();
                for (int j=0; j<rowNumber; j++){
                    col.elements.add(tmpData[j]);
                }
                columns.add(col);
            } else if ("Double[]".equals(type)) {
                col = new Column<Double>();
                Double[] tmpData = (Double[]) data.get(i);
                col.label = labels.get(i);
                col.elements = new ArrayList<Double>();
                for (int j=0; j<rowNumber; j++){
                    col.elements.add(tmpData[j]);
                }
                columns.add(col);
            }else{
                col = new Column<Integer[]>();
            }
        }
    }

    public Dataframe(String pathToCsv) throws IOException {

        BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
        String[] labels;
        String[] types;
        String row;

        if ((row = csvReader.readLine()) != null) {
            labels = row.split(",");


            if ((row = csvReader.readLine()) != null) {
                types = row.split(",");

                Column col;
                columns = new ArrayList<Column>();
                for (int i = 0; i < labels.length; i++) {
                    String type = types[i];
                    if ("Integer".equals(type)) {
                        col = new Column<Integer>();
                        col.label = labels[i];
                        col.elements = new ArrayList<Integer>();
                        columns.add(col);
                    } else if ("String".equals(type)) {
                        col = new Column<String>();
                        col.label = labels[i];
                        col.elements = new ArrayList<String>();
                        columns.add(col);
                    } else if ("Double".equals(type)) {
                        col = new Column<Double>();
                        col.label = labels[i];
                        col.elements = new ArrayList<Double>();
                        columns.add(col);
                    } else {
                        col = new Column<Integer>();
                    }
                }

                while ((row = csvReader.readLine()) != null) {
                    String[] data = row.split(",");
                    for(int i=0; i<data.length; i++){
                        if ("Integer".equals(types[i])) {
                            if(data[i].length() == 0){
                                columns.get(i).elements.add(0);
                            }else{
                                columns.get(i).elements.add(Integer.parseInt(data[i]));
                            }
                        } else if ("String".equals(types[i])) {
                            columns.get(i).elements.add(data[i]);
                        } else if ("Double".equals(types[i])) {
                            if(data[i].length() == 0){
                                columns.get(i).elements.add(0.0);
                            }else{
                                columns.get(i).elements.add(Double.parseDouble(data[i]));
                            }
                        } else {
                            columns.get(i).elements.add(data[i]);
                        }
                    }
                }
            }
        }
        csvReader.close();
    }

    public int[] maxWidthPerColumn(){
        int widths[];
        int current=0;
        widths = new int[columns.size()];
        for(int i = 0; i<columns.size(); i++){
            widths[i] = columns.get(i).label.length();
        }
        for(int i = 0; i<columns.size(); i++){
            for(int j=0; j<columns.get(i).elements.size(); j++){
                current = columns.get(i).elements.get(j).toString().length();
                if(current> widths[i]){
                    widths[i] = current;
                }
            }
        }
        return widths;
    }

    public String getDataFrameContent(){
        int[] widths = maxWidthPerColumn();
        int spaces = 0;
        String s = new String();
        for (int i=-1; i<columns.size(); i++){
            if(i==-1){
                s += "index|";
            }else {
                s += columns.get(i).label;
                spaces = widths[i]-columns.get(i).label.length();
                for(int k=0; k<spaces; k++){
                    s += " ";
                }
                s += "|";
            }
        }
        s += "\n";
        for (int j=0; j<columns.get(0).elements.size(); j++){
            for (int i=-1; i<columns.size(); i++){
                if(i==-1){
                    s += j;
                    spaces = 5-Integer.toString(j).length();
                    for(int k=0; k<spaces; k++){
                        s += " ";
                    }
                    s += "|";
                }else {
                    s += columns.get(i).elements.get(j);
                    spaces = widths[i]-columns.get(i).elements.get(j).toString().length();
                    for(int k=0; k<spaces; k++){
                        s += " ";
                    }
                    s += "|";
                }
            }
            s += "\n";
        }
        return s;
    }

    public void print(){
        System.out.println(getDataFrameContent());
    }

    public void printBeginning(int nbLines){
        String wholeDf = getDataFrameContent();
        String labels = wholeDf.substring(0, wholeDf.indexOf("\n")+1);
        System.out.print(labels);
        int count = 0;
        int index = wholeDf.indexOf("\n") + 1;
        while(index<wholeDf.length() && count != nbLines){
            if(wholeDf.charAt(index) == '\n'){
                count++;
            }
            index++;
        }
        System.out.println(wholeDf.substring(wholeDf.indexOf("\n")+1, index));

    }

    public void printEnd(int nbLines){
        String wholeDf = getDataFrameContent();
        String labels = wholeDf.substring(0, wholeDf.indexOf("\n")+1);
        System.out.print(labels);
        int count = -1;
        int index = wholeDf.length()-1;
        while(index>wholeDf.indexOf("\n")+1 && count != nbLines){
            if(wholeDf.charAt(index) == '\n'){
                count++;
            }
            index--;
        }
        index++;
        System.out.println(wholeDf.substring(index+1));
    }

    public Dataframe selectFromDataFrame(String[] labels, int[] indexes){

        Dataframe newDf = new Dataframe();

        for(int i=0; i<labels.length; i++){
            newDf.columns.add(new Column());
            newDf.columns.get(i).label = labels[i];
            int indexCol = findColumnIndex(labels[i]);
            newDf.columns.get(i).elements = new ArrayList();
            for(int j=0; j<indexes.length; j++){
                newDf.columns.get(i).elements.add(columns.get(indexCol).elements.get(indexes[j]));
            }
        }

        return newDf;
    }

    public int findColumnIndex(String label){
        int i=0;
        while(i<columns.size() && !columns.get(i).label.equals(label)){
            i++;
        }
        if(i<columns.size()) {
            return i;
        }else{
            return -1;
        }
    }

    public int maxInteger(String label) {
        int colIndex = findColumnIndex(label);
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < columns.get(colIndex).elements.size(); i++) {
            if (max < (int) columns.get(colIndex).elements.get(i)){
                max = (int) columns.get(colIndex).elements.get(i);
            }
        }
        return max;
    }

    public double maxDouble(String label) {
        int colIndex = findColumnIndex(label);
        double max = Double.MIN_VALUE;
        for (int i = 0; i < columns.get(colIndex).elements.size(); i++) {
            if (max < (double) columns.get(colIndex).elements.get(i)){
                max = (double) columns.get(colIndex).elements.get(i);
            }
        }
        return max;
    }

    public int minInteger(String label) {
        int colIndex = findColumnIndex(label);
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < columns.get(colIndex).elements.size(); i++) {
            if (min > (int) columns.get(colIndex).elements.get(i)){
                min = (int) columns.get(colIndex).elements.get(i);
            }
        }
        return min;
    }

    public double minDouble(String label) {
        int colIndex = findColumnIndex(label);
        double min = Double.MAX_VALUE;
        for (int i = 0; i < columns.get(colIndex).elements.size(); i++) {
            if (min > (double) columns.get(colIndex).elements.get(i)){
                min = (double) columns.get(colIndex).elements.get(i);
            }
        }
        return min;
    }

    public double meanInteger(String label){
        int colIndex = findColumnIndex(label);
        double sum = 0.;
        for (int i = 0; i < columns.get(colIndex).elements.size(); i++) {
            sum += (int) columns.get(colIndex).elements.get(i);
        }
        return sum/columns.get(colIndex).elements.size();
    }

    public double meanDouble(String label){
        int colIndex = findColumnIndex(label);
        double sum = 0.;
        for (int i = 0; i < columns.get(colIndex).elements.size(); i++) {
            sum += (double) columns.get(colIndex).elements.get(i);
        }
        return sum/columns.get(colIndex).elements.size();
    }

    public int medianInteger(String label){
        int colIndex = findColumnIndex(label);
        ArrayList<Integer> elementsSorted = (ArrayList<Integer>) columns.get(colIndex).elements.clone();
        Collections.sort(elementsSorted);
        int indexMedian = elementsSorted.size()/2;
        return elementsSorted.get(indexMedian).intValue();
    }

    public double medianDouble(String label){
        int colIndex = findColumnIndex(label);
        ArrayList<Double> elementsSorted = (ArrayList<Double>) columns.get(colIndex).elements.clone();
        Collections.sort(elementsSorted);
        int indexMedian = elementsSorted.size()/2;
        return elementsSorted.get(indexMedian);
    }

    public Dataframe sample(String[] labels, int nbRow) throws InputMismatchException{
        if(columns.size() == 0 || columns.get(0).elements.size() < nbRow){
            throw new InputMismatchException();
        }
        ArrayList<Integer> indexes = new ArrayList<Integer>();
        int index;

        while(indexes.size() < nbRow){
            double random = Math.random();
            index = (int) (random * (double) columns.get(0).elements.size());
            if(!indexes.contains(index)){
                indexes.add(index);
            }
        }

        int[] res = new int[nbRow];
        for(int i=0; i<nbRow; i++){
                res[i]= indexes.get(i);
        }

        return selectFromDataFrame(labels, res);
    }
}
