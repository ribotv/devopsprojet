import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");
        String[] label = {df.columns.get(0).label, df.columns.get(2).label};
        int[] indexes = {0, 2};
        Dataframe subDf = df.selectFromDataFrame(label, indexes);
        subDf.print();
        TestDataframe tdf = new TestDataframe();

        tdf.testSelectFromDataFrame();
        tdf.testMeanInteger();
        tdf.testMeanDouble();
        tdf.testMaxDouble();
        tdf.testMaxInteger();

        tdf.testMinDouble();
        tdf.testMinInteger();
        tdf.testMedianInteger();
        tdf.testMedianDouble();

        tdf.testSample();
    }
}
