import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.*;
import static org.junit.Assert.*;

public class TestDataframe {

    @Test
    public void testConstruct1() {
        ArrayList<String> labels = new ArrayList<String>();
        labels.add("label1");
        labels.add("label2");
        labels.add("label3");

        ArrayList data = new ArrayList();
        String[] s = {"yoyo", "yaya", "siii"};
        data.add(s);
        Integer[] i = {1, 2, 3};
        data.add(i);
        Double[] f = {0.5, 9.6, 6.8};
        data.add(f);

        Dataframe df = new Dataframe(labels, data, 3);


        Column column0 = df.columns.get(0);
        assertEquals(column0.label, "label1");
        assertEquals(column0.elements.get(0), "yoyo");
        assertEquals(column0.elements.get(1), "yaya");
        assertEquals(column0.elements.get(2), "siii");

        Column column1 = df.columns.get(1);
        assertEquals(column1.label, "label2");
        assertEquals(column1.elements.get(0), (Integer) 1);
        assertEquals(column1.elements.get(1), (Integer) 2);
        assertEquals(column1.elements.get(2), (Integer) 3);

        Column column2 = df.columns.get(2);
        assertEquals(column2.label, "label3");
        assertEquals(column2.elements.get(0), (Double) 0.5);
        assertEquals(column2.elements.get(1), (Double) 9.6);
        assertEquals(column2.elements.get(2), (Double) 6.8);

    }

    @Test
    public void testConstruct2() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");


        Column column0 = df.columns.get(0);
        assertEquals(column0.label, "label1");
        assertEquals(column0.elements.get(0), "yoyo");
        assertEquals(column0.elements.get(1), "yaya");
        assertEquals(column0.elements.get(2), "siii");

        Column column1 = df.columns.get(1);
        assertEquals(column1.label, "label2");
        assertEquals(column1.elements.get(0), (Integer) 1);
        assertEquals(column1.elements.get(1), (Integer) 2);
        assertEquals(column1.elements.get(2), (Integer) 3);

        Column column2 = df.columns.get(2);
        assertEquals(column2.label, "label3");
        assertEquals(column2.elements.get(0), (Double) 0.5);
        assertEquals(column2.elements.get(1), (Double) 9.6);
        assertEquals(column2.elements.get(2), (Double) 6.8);

    }

    @Test
    public void testConstestGetDataFrameContent() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        String content = df.getDataFrameContent();
        assertEquals(content, "index|label1|label2|label3|\n0    |yoyo  |1     |0.5   |\n1    |yaya  |2     |9.6   |\n2    |siii  |3     |6.8   |\n");

    }

    @Test
    public void testSelectFromDataFrame() {
        ArrayList<String> labels = new ArrayList<String>();
        labels.add("label1");
        labels.add("label2");
        labels.add("label3");

        ArrayList data = new ArrayList();
        String[] s = {"yoyo", "yaya", "siii"};
        data.add(s);
        Integer[] i = {1, 2, 3};
        data.add(i);
        Double[] f = {0.5, 9.6, 6.8};
        data.add(f);

        Dataframe df = new Dataframe(labels, data, 3);
        String[] colLabels = {"label1", "label3"};
        int[] indexes = {0, 2};
        Dataframe subDf = df.selectFromDataFrame(colLabels, indexes);


        Column column0 = subDf.columns.get(0);
        assertEquals(column0.label, "label1");
        assertEquals(column0.elements.get(0), "yoyo");
        assertEquals(column0.elements.get(1), "siii");

        Column column2 = subDf.columns.get(1);
        assertEquals(column2.label, "label3");
        assertEquals(column2.elements.get(0), (Double) 0.5);
        assertEquals(column2.elements.get(1), (Double) 6.8);

        assertEquals(subDf.columns.size(), 2);
        assertEquals(subDf.columns.get(0).elements.size(), 2);
    }

    @Test
    public void testMaxInteger() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        assertEquals(df.maxInteger("label2"), 3);
    }

    @Test
    public void testMaxDouble() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        assert(df.maxDouble("label3") == 9.6);
    }

    @Test
    public void testMinInteger() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        assertEquals(df.minInteger("label2"), 1);
    }

    @Test
    public void testMinDouble() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        assert(df.minDouble("label3")== 0.5);
    }

    @Test
    public void testMeanInteger() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        assert(df.meanInteger("label2") == 2.0);
    }

    @Test
    public void testMeanDouble() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        assert(df.meanDouble("label3") == (0.5 + 9.6 + 6.8) / 3);
    }

    @Test
    public void testMedianInteger() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        assertEquals(df.medianInteger("label2"), 2);
    }

    @Test
    public void testMedianDouble() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        assert(df.medianDouble("label3") == 6.8);
    }
    @Test
    public void testSample() throws IOException {
        String projectPath = System.getProperty("user.dir");
        Dataframe df = new Dataframe(projectPath + "/src/test/dummyCSV.csv");

        String[] colLabels = {"label1", "label3"};
        assert(df.sample(colLabels, 2).columns.get(0).elements.size() == 2);
    }
}